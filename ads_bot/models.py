from django.db import models


class ChatIds(models.Model):
    chat_id = models.IntegerField()

    def __str__(self):
        return str(self.chat_id)
