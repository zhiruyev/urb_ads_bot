from telebot import TeleBot

from ads_bot.models import ChatIds


class BotSubsHandler:

    def create_bot_sub(self, chat_id: int) -> None:
        ChatIds.objects.create(chat_id=chat_id)

    def send_greeting_message(self, bot: TeleBot, chat_id: int) -> None:
        bot.send_message(chat_id, 'Привет! Я бот, который будет сообщать тебе о событиях в сфере ЖКХ. ')
