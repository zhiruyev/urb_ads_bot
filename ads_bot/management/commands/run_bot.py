from django.core.management.base import BaseCommand

import telebot

from django.conf import settings

from ads_bot.handlers.bot_subs_handler import BotSubsHandler

bot = telebot.TeleBot(settings.BOT_TOKEN)


class Command(BaseCommand):
    help = 'Displays current time'

    def handle(self, *args, **kwargs):
        print("Bot is running...")

        @bot.message_handler(commands=['start'])
        def start(message: telebot.types.Message):
            BotSubsHandler().create_bot_sub(message.chat.id)
            BotSubsHandler().send_greeting_message(bot, message.chat.id)

        bot.polling()
