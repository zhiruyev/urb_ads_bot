from django.apps import AppConfig


class AdsBotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ads_bot'
